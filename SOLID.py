class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


class Learn:
    def action(self):
        return 'Studying science'


class Teach:
    def action(self):
        return 'Teaches students'


class Action:
    def action(self, notifier):
        return notifier().action()


class Student(Person, Action):
    def __init__(self, first_name, last_name, student_id):
        super().__init__(first_name, last_name)
        self.first_name = first_name
        self.last_name = last_name
        self.student_id = student_id

    def __str__(self):
        return f'Name: {self.first_name} {self.last_name}, Student_id: {self.student_id}'


class Teacher(Person, Action):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self):
        return f'Name: {self.first_name} {self.last_name}'


class Group:
    def __init__(self):
        self.student_list = []
        self.teacher_list = []
        self.separator = ', '

    def add_student(self, student):
        if student in self.student_list:
            print('-- The student is already in the group.')
        else:
            self.student_list.append(student)
            print(f"-- Student {student.first_name + ' ' + student.last_name} added to group")

    def del_student(self, student):
        if student in self.student_list:
            del self.student_list[self.student_list.index(student)]
            print(f"-- Student {student.first_name + ' ' + student.last_name} removed from the group")
        else:
            print('No such student')

    def add_teacher(self, teacher):
        if teacher in self.teacher_list:
            print('-- The teacher is already in the group.')
        else:
            self.teacher_list.append(teacher)
            print(f"-- Teacher {teacher.first_name + ' ' + teacher.last_name} added to group")

    def del_teacher(self, teacher):
        if teacher in self.teacher_list:
            del self.teacher_list[self.teacher_list.index(student)]
            print(f"-- Teacher {teacher.first_name + ' ' + teacher.last_name} removed from the group")
        else:
            print('No such teacher')

    def __str__(self):
        return f"Students: {self.separator.join([i.first_name + ' ' + i.last_name for i in self.student_list])}\n" \
               f"Teachers: {self.separator.join([i.first_name + ' ' + i.last_name for i in self.teacher_list])}"

class PipeSeparator(Group):
    def __init__(self):
        super().__init__()
        self.separator = '| '


student1 = Student('Yurii', 'Ostapenko', '13')
student2 = Student('Katya', 'Khmara', '52')
student3 = Student('Oleh', 'Skrypka', '35')
#
teacher1 = Teacher('Master', 'Yoda')
#
group1 = Group()
group2 = Group()
#
group1.add_student(student2)
group1.add_student(student1)
group1.add_teacher(teacher1)
print(group1)

group3 = PipeSeparator()

group3.add_student(student2)
group3.add_student(student1)
group3.add_student(student3)
print(teacher1.action(Teach))
