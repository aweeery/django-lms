from django.core.management.base import BaseCommand

import random
from faker import Faker

fake = Faker()

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('quantity', type=int)

    def generate_students(quantity):
        students_list = [
            {
                "First_name": fake.first_name(),
                "Last_name": fake.last_name(),
                "Email": fake.unique.email(),
                "Password": fake.password(length=random.randint(10, 25)),
                "Birthday": str(fake.date_of_birth(minimum_age=18, maximum_age=45))
            } for _ in range(quantity)
        ]
        return students_list

    def handle(self, *args, **options):
        for student in Command.generate_students(options['quantity']):
            self.stdout.write(student['First_name'])